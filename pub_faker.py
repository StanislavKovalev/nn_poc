#!/usr/bin/env python3
import time
import json
from random import random, randint
from http.client import HTTPConnection
from common_code import localhost, randhash

def join_param(param):
    return '='.join([param['label'], randhash(param['size'])])

def generate_search_query(params):
    query_params = '&'.join(
        [join_param(param) for param in params]
    )

    return '/s?' + query_params

if __name__ == '__main__':
    config = json.load(open('config.json'))
    while True:
        try:
            connection = HTTPConnection(localhost(config['nn_serv_port']))
            search_query = generate_search_query(config['query_params'])
            connection.request('GET', search_query)
            print('Query sent %s' % search_query)
            time.sleep(random() / 3)
        except ConnectionRefusedError:
            print('Connection refused, retrying...')
            time.sleep(2)
