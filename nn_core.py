import time
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras import regularizers
import numpy as np
from threading import Thread


class NNClickPredict:
  _queries_batch = {}
  _feature_sizes = {}
  _n_clicks_received = 0
  _clf_inititalized = False

  def __init__(self,
                features_descr,
                regularization,
                layers_sizes=(15, 10),
                activation = 'tanh',
                batching_time=300):
    self._map_feature_sizes(features_descr)
    self._get_input_size(features_descr)

    self.batching_time = batching_time * 1000
    self._prev_batch_learn_ts = self._get_ts()

    self.clf = Sequential([
      Dense(layers_sizes[0],
            input_dim=self._input_size,
            kernel_regularizer=regularizers.l1(regularization)),
      Activation(activation),
      Dense(layers_sizes[1], kernel_regularizer=regularizers.l1(regularization)),
      Activation(activation),
      Dense(1),
      Activation(activation),
    ])

    self.clf.compile(optimizer='adamax',
                     loss='binary_crossentropy',
                     metrics=['accuracy'])

  def get_click_proba(self, query):
    features = [param.split('=') for param in query.split('&')]
    nn_input = self._hash_features(features)

    if not self._clf_inititalized:
      self.clf.fit(nn_input, [0.5])
      self._clf_inititalized = True

    return self.clf.predict(nn_input)[0, 0]

  def mark_as_clicked(self, click_id):
    print('handle_clicked_query %s' % click_id)

    click_id = int(click_id)
    if click_id in self._queries_batch:
      query = self._queries_batch[int(click_id)]
      query['clicked'] = 1
      self._n_clicks_received += 1
      print('Received %d clicks' % self._n_clicks_received)

      current_ts = self._get_ts()
      if current_ts - self._prev_batch_learn_ts > 2 * self.batching_time:
        self._learn_nn()
    else:
      print('query not found for click_id %s' % str(click_id))

  def add_to_batch(self, query, click_id):
    features = [param.split('=') for param in query.split('&')]
    hashed = self._hash_features(features)

    self._queries_batch[int(click_id)] = {
      'query': hashed,
      'clicked': 0,
      'ts': self._get_ts()
    }

    print('Batch size %d' % len(self._queries_batch))


  def _map_feature_sizes(self, features_descr):
    for feature in features_descr:
      self._feature_sizes[feature['label']] = feature['size']

  def _get_input_size(self, features_descr):
    self._input_size = 0
    for feature in features_descr:
      self._input_size += feature['size']

  def _hash_features(self, features):
    return np.array(
      [self._hash_to_binary(*feature) for feature in features], dtype='short'
    ).flatten().reshape(1, self._input_size)

  def _hash_to_binary(self, label, value):
    size = self._feature_sizes[label]
    return list(bin(int(value))[2:].zfill(size))

  def _learn_nn(self):
    ts_cond = self._prev_batch_learn_ts + self.batching_time
    self._prev_batch_learn_ts = ts_cond
    batch = { cid: entry for cid, entry in self._queries_batch.items() if entry['ts'] < ts_cond }

    Thread(None, self._train_on_batch, args=[batch]).start()
  
  def _train_on_batch(self, batch):
    print('start training nn, batch size is %d' % len(batch))
    batch_size = len(batch)
    queries = np.array(
      [entry['query'] for entry in batch.values()]
    ).reshape(batch_size, self._input_size)
    
    target = np.array(
      [entry['clicked'] for entry in batch.values()]
    ).reshape(batch_size, 1)

    self.clf.fit(queries, target)
    print('training finished')
    self._cleanup_batch()

  def _cleanup_batch(self):
    print('start batch cleanup')
    self._queries_batch = {cid: entry for cid,
                           entry in self._queries_batch.items()
                           if entry['ts'] >= self._prev_batch_learn_ts}
    print('batch cleaned, batch length is %d' % len(self._queries_batch))

  def _get_ts(self):
    '''
    Returns timestamp in milliseconds
    '''
    return int(time.time() * 1000)

