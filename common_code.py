from random import random, randint

def localhost(port):
    return 'localhost:' + str(port)

def randhash(size):
    return str(randint(0, 2 ** size))
