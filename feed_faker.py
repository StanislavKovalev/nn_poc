#!/usr/bin/env python3
import json
from http.client import HTTPConnection
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Timer
from random import random
from common_code import localhost


def ClickRequest(click_query, nn_server_addr):
  print('Send click query %s' % click_query)
  connection = HTTPConnection(nn_server_addr)
  connection.request('GET', '/c?' + click_query)

def FeedFakerFactory(nn_server_addr):
  class FeedFaker(BaseHTTPRequestHandler):
    def do_GET(self):
      query = self.requestline.split()[1]
      if random() > 0.5:
        print('Feed got query %s' % query)
        Timer(random() * 30, ClickRequest, [query, nn_server_addr]).start()

  return FeedFaker

if __name__ == '__main__':
  config = json.load(open('config.json'))
  serv = HTTPServer(
    ('localhost', config['feed_port']),
    FeedFakerFactory(localhost(config['nn_serv_port']))
  )
  print('Starting server, use <Ctrl-C> to stop')
  serv.serve_forever()
