#!/usr/bin/env python3
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
from http.client import HTTPConnection
from nn_core import NNClickPredict
from common_code import localhost, randhash
import re


def QueryProcessorFactory(nn, treshold, feed_addr):
  class QueryProcessor(BaseHTTPRequestHandler):
    def do_GET(self):
      request_data = self.requestline.split()[1].split('?')

      if len(request_data) < 2:
        self.send_error('No params passed in query')
        return

      resource = request_data[0]
      query = request_data[1]

      if resource == '/s':
        self.handle_search(query)
      elif resource == '/c':
        self.handle_click(query)
      else:
        self.send_error('Resource not found')
        return

      # self.send_response(200)
      # self.send_header('content-type', 'text/html')
      # self.end_headers()

    def send_error(self, error):
      self.send_response(400)
      self.send_header('content-type', 'text/html')
      self.end_headers()


    def handle_search(self, query):
      click_proba = nn.get_click_proba(query)

      if click_proba >= treshold:
        click_id = randhash(64)
        feed_query = query + '&click_id=' + click_id
        nn.add_to_batch(query, click_id)
        # print('Sending query to feed... %s' % feed_query)
        connection = HTTPConnection(feed_addr)
        connection.request('GET', feed_query)
        # send request to feed if click predicted

    def handle_click(self, query):
      cid_search = re.search(r'click_id=(\d+)', query)
      if cid_search:
        nn.mark_as_clicked(cid_search.group(1))

  return QueryProcessor


if __name__ == '__main__':
  config = json.load(open('config.json'))

  nn_click_predict = NNClickPredict(features_descr = config['query_params'],
                                    regularization = config['regularization'],
                                    layers_sizes=config['layers_sizes'],
                                    activation=config['activation'],
                                    batching_time=config['batching_time'])
  query_processor = QueryProcessorFactory(
    nn_click_predict, 0, localhost(config['feed_port'])
  )

  serv = HTTPServer(('localhost', config['nn_serv_port']), query_processor)
  print('Starting server, use <Ctrl-C> to stop')
  serv.serve_forever()
